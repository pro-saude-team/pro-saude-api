<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Doctor extends Model
{
    use SoftDeletes;

    protected $fillable = ['name', 'crm', 'email', 'clinic_id'];

    /**
     * Get the clinic that owns the doctor.
     */
    public function clinic()
    {
        return $this->belongsTo('App\Clinic');
    }
}
