<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Schedule extends Model
{
    use SoftDeletes;

    protected $fillable = ['date', 'status', 'doctor_id', 'patient_id'];

    /**
     * Get the doctor associated with scheduling.
     */
    public function doctor()
    {
        return $this->belongsTo('App\Doctor');
    }

    /**
     * Get the patient associated with scheduling.
     */
    public function patient()
    {
        return $this->belongsTo('App\Patient');
    }
}
