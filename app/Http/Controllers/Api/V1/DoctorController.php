<?php

namespace App\Http\Controllers\Api\V1;

use App\Doctor;
use App\Http\Controllers\Controller;
use App\Http\Resources\DoctorResource;
use Illuminate\Http\Request;

class DoctorController extends Controller
{
    /**
     * Display a listing of the doctors.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return DoctorResource::collection(Doctor::paginate(15));
    }

    /**
     * Store a newly created doctor in database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'name' => 'required|string',
            'crm' => 'required|string',
            'email' => 'required|email',
            'clinic_id' => 'exists:clinics,id'
        ]);

        $doctor = Doctor::create($data);

        return response()->Json(new DoctorResource($doctor), 201);
    }

    /**
     * Return the specified doctor.
     *
     * @param  \App\Doctor  $doctor
     * @return \Illuminate\Http\Response
     */
    public function show(Doctor $doctor)
    {
        $doctor = Doctor::findOrFail($doctor->id);

        return new DoctorResource($doctor);
    }

    /**
     * Update the specified doctor in database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Doctor  $doctor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Doctor $doctor)
    {
        $data = $request->validate([
            'name' => 'string',
            'crm' => 'string',
            'email' => 'email',
            'clinic_id' => 'exists:clinics,id'
        ]);
        $doctor->update($data);

        return new DoctorResource($doctor);
    }

    /**
     * Remove the specified doctot from database.
     *
     * @param  \App\Doctor  $doctor
     * @return \Illuminate\Http\Response
     */
    public function destroy(Doctor $doctor)
    {
        $doctor->delete();
        return response()->json(null, 204);
    }
}
