<?php

namespace App\Http\Controllers\Api\V1;

use App\Clinic;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\ClinicResource;
use App\Http\Resources\ClinicResourceCollection;

class ClinicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return ClinicResource::collection(Clinic::paginate(15));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'name' => 'required|string',
            'cnes' => 'required|string',
            'owner' => 'required|string',
            'cnpj' => 'required|string',
        ]);
        $clinic = Clinic::create($data);

        return response()->Json(new ClinicResource($clinic), 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Clinic  $clinic
     * @return \Illuminate\Http\Response
     */
    public function show(Clinic $clinic)
    {
        $clinic = Clinic::findOrFail($clinic->id);

        return response()->Json(new ClinicResource($clinic), 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Clinic  $clinic
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Clinic $clinic)
    {
        $data = $request->validate([
            'name' => 'nullable|string',
            'cnes' => 'nullable|string',
            'owner' => 'nullable|string',
            'cnpj' => 'nullable|string',
        ]);
        $clinic->update($data);

        return response()->Json(new ClinicResource($clinic), 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Clinic  $clinic
     * @return \Illuminate\Http\Response
     */
    public function destroy(Clinic $clinic)
    {
        $clinic->delete();
        return response()->json(null, 204);
    }
}
