<?php

namespace App\Http\Controllers\Api\V1;

use App\Schedule;
use App\Http\Controllers\Controller;
use App\Http\Resources\ScheduleResource;
use Illuminate\Http\Request;

class ScheduleController extends Controller
{
    /**
     * Return a listing of the schedules.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return ScheduleResource::collection(Schedule::paginate(15));
    }

    /**
     * Store a newly created doctor in database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'date' => 'required|date_format:d-m-Y|after:yesterday',
            'status' => 'required|integer|min:0|max:2',
            'doctor_id' => 'required|exists:doctors,id',
            'patient_id' => 'required|exists:patients,id',
        ]);

        $schedule = Schedule::create($data);

        return response()
            ->Json(['data' => new ScheduleResource($schedule)], 201);
    }

    /**
     * Return the specified schedule.
     *
     * @param  \App\Schedule  $schedule
     * @return \Illuminate\Http\Response
     */
    public function show(Schedule $schedule)
    {
        return new ScheduleResource($schedule);
    }

    /**
     * Update the specified schedule in database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Schedule  $schedule
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Schedule $schedule)
    {
        $data = $request->validate([
            'date' => 'date_format:d-m-Y|after:yesterday',
            'status' => 'integer|min:0|max:2',
            'doctor_id' => 'exists:doctors,id',
            'patient_id' => 'exists:patients,id',
        ]);
        $schedule->update($data);

        return new ScheduleResource($schedule);
    }

    /**
     * Remove the specified doctor from database.
     *
     * @param  \App\Patient  $schedule
     * @return \Illuminate\Http\Response
     */
    public function destroy(Schedule $schedule)
    {
        $schedule->delete();
        return response()->json(null, 204);
    }
}
