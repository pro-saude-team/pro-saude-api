<?php

namespace App\Http\Controllers\Api\V1;

use App\Patient;
use App\Http\Controllers\Controller;
use App\Http\Resources\PatientResource;
use Illuminate\Http\Request;

class PatientController extends Controller
{
    /**
     * Return a listing of the patients.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return PatientResource::collection(Patient::paginate(15));
    }

    /**
     * Store a newly created doctor in database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'name' => 'required|string',
            'age' => 'required|integer|min:0|max:150',
            'sex' => 'required|integer|min:0|max:1',
            'email' => 'required|email',
        ]);

        $patient = Patient::create($data);

        return response()->Json(new PatientResource($patient), 201);
    }

    /**
     * Return the specified patient.
     *
     * @param  \App\Patient  $patient
     * @return \Illuminate\Http\Response
     */
    public function show(Patient $patient)
    {
        $patient = Patient::findOrFail($patient->id);

        return new PatientResource($patient);
    }

    /**
     * Update the specified patient in database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Patient  $patient
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Patient $patient)
    {
        $data = $request->validate([
            'name' => 'string',
            'age' => 'integer|min:0|max:150',
            'sex' => 'integer|min:0|max:1',
            'email' => 'email',
        ]);
        $patient->update($data);

        return new PatientResource($patient);
    }

    /**
     * Remove the specified doctor from database.
     *
     * @param  \App\Patient  $patient
     * @return \Illuminate\Http\Response
     */
    public function destroy(Patient $patient)
    {
        $patient->delete();
        return response()->json(null, 204);
    }
}
