<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'v1', 'namespace' => '\App\Http\Controllers\Api\V1'], function () {
    Route::apiResource('clinics', 'ClinicController');
    Route::apiResource('doctors', 'DoctorController');
    Route::apiResource('patients', 'PatientController');
    Route::apiResource('schedules', 'ScheduleController');
    Route::group(['middleware' => 'auth:api'], function () {
    });

    Route::fallback(function () {
        return response()->json([
            'message' => 'Page Not Found'
        ], 404);
    });
});
