## Sobre a API do Pró-Saúde

Trata-se de uma API pública para o gerenciamento de recursos do sistema de gerência para clínicas Pró-Saúde. Dentre os recursos disponíveis estão:

- CRUD de clínicas
- CRUD de médicos
- CRUD de especialidades dos médicos
- CRUD de agendamento de consultas

Todas esses recursos são acessíveis pelas rotas base abaixo, de modo a seguirem os 4 verbos padrões HTTP (GET, UPDATE, PUT, DELETE). Para exemplificar o CRUD de clínicas possuí as seguintes rotas:

- http://localhost/pro-saude/public/api/v1/clinics (POST) - Registra uma nova clínica;
- http://localhost/pro-saude/public/api/v1/clinics (GET) - Retorna todas as clínicas cadastradas;
- http://localhost/pro-saude/public/api/v1/clinics/{id} (GET) - Retorna todas a clínica com o {id} especificado;
- http://localhost/pro-saude/public/api/v1/clinics/{id} (PUT) - Atualiza os dados de uma clínica com o {id} especificado;
- http://localhost/pro-saude/public/api/v1/clinics/{id} (DELETE) - Exclui os dados de uma clínica com o {id} especificado.

Todos os outros recursos seguem a mesma lógica com a seguinte url base:

- http://localhost/pro-saude/public/api/v1/doctors;
- http://localhost/pro-saude/public/api/v1/specialties;
- http://localhost/pro-saude/public/api/v1/pacients;
- http://localhost/pro-saude/public/api/v1/schedules.


## Instalação

A instalação é bastante simples. Basta clonar este repositório, renomear o arquivo .env.example para .env e definir as configurações do seu banco de dados nos campos indicados.
Após isso os seguintes comando devem ser rodados em série, dentro da pasta:

- composer install #instalação prévia do composer necessária (https://getcomposer.org/doc/00-intro.md)
- php artisan key:generate
- php artisan migrate:fresh #isto apagará as tabelas do banco atual e construirá as mesmas de novo
- php artisan serve #levanta um servidor de testes (nunca utilize para produção)

Adicionalmente você também pode correr a suíte de teste autmatizados incluídas neste projeto:

- ./vendor/bin/phpunit

E testar se está tudo funcionando com alguns dados falsos:

- php artisan db:seed


