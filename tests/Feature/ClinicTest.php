<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ClinicTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
        $this->withoutExceptionHandling();
    }
    /**
     * Test for creating a basic clinic
     *
     * @test
     * @return void
     */
    public function canCreateAClinic()
    {
        $clinic = factory('App\Clinic')->make();
        $response = $this->post('/api/v1/clinics', $clinic->toArray());

        $response->assertStatus(201);
        $response->assertJson($clinic->toArray());
        $this->assertDatabaseHas('clinics', $clinic->toArray());
    }

    /**
     * Test to return a specific clinic that has
     * been updated.
     *
     * @test
     * @return void
     */
    public function canUpdateAClinic()
    {
        $clinic = factory('App\Clinic')->create();


        $clinic->name = $clinic->name . "_updated";
        $clinic->owner = $clinic->owner . "_updated";

        $validate = $clinic->toArray();

        unset($validate['created_at']);
        unset($validate['updated_at']);

        $response = $this->put("/api/v1/clinics/$clinic->id", $clinic->toArray());

        $response->assertStatus(200);
        $response->assertJson($validate);
        $this->assertDatabaseHas('clinics', [
                                                "name" => $validate['name'],
                                                "owner" => $validate['owner'],
                                            ]);
    }

    /**
     * Test for return a specific clinic 
     * exsisting in database.
     *
     * @test
     * @return void
     */
    public function canReturnAClinic()
    {
        $clinic = factory('App\Clinic')->create();

        $validate = $clinic->toArray();

        unset($validate['created_at']);
        unset($validate['updated_at']);

        $response = $this->get("/api/v1/clinics/$clinic->id");

        $response->assertStatus(200);
        $response->assertJson($validate);
    }

    /**
     * Test to assert if an existing clinic
     * can be deleted.
     *
     * @test
     * @return void
     */
    public function canDropAClinic()
    {
        $clinic = factory('App\Clinic')->create();

        $response = $this->delete("/api/v1/clinics/$clinic->id");

        $response->assertStatus(204);
        $this->assertSoftDeleted('clinics', ["id" => $clinic->id]);
    }

    /**
     * Test for return all clinics
     * exsisting in database.
     *
     * @test
     * @return void
     */
    public function canReturnAllClinics()
    {
        $clinics = factory('App\Clinic', 5)->create();

        $response = $this->get("/api/v1/clinics");

        $response->assertJsonStructure([
            "data" => [
                ["id", "name", "cnes", "owner", "cnpj"],
            ], "links", "meta",
        ]);

        $response->assertStatus(200);
    }
}
