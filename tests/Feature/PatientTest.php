<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class PatientTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
        $this->withoutExceptionHandling();
    }
    /**
     * Test for creating a patient
     *
     * @test
     * @return void
     */
    public function canCreateAPatient()
    {
        $patient = factory('App\Patient')->make();

        $response = $this->post('/api/v1/patients', $patient->toArray());

        $data = $patient->toArray();

        $response->assertStatus(201);
        $response->assertJson($data);
        $this->assertDatabaseHas('patients', $patient->toArray());
    }

    /**
     * Test to return a specific patient that has
     * been updated.
     *
     * @test
     * @return void
     */
    public function canUpdateAPatient()
    {
        $patient = factory('App\Patient')->create();


        $patient->name = $patient->name . "_updated";
        $patient->email = $patient->email . "_updated";

        $validate = $patient->toArray();

        unset($validate['created_at']);
        unset($validate['updated_at']);

        $response = $this->put("/api/v1/patients/$patient->id", $patient->toArray());

        $response->assertStatus(200);
        $response->assertJson(['data' => $validate]);
        $this->assertDatabaseHas('patients', [
                                                'name' => $validate['name'],
                                                'email' => $validate['email'],
                                            ]);
    }

    /**
     * Test for return a specific patient 
     * exsisting in database.
     *
     * @test
     * @return void
     */
    public function canReturnAPatient()
    {
        $patient = factory('App\Patient')->create();

        $validate = $patient->toArray();

        unset($validate['created_at']);
        unset($validate['updated_at']);

        $response = $this->get("/api/v1/patients/$patient->id");

        $response->assertStatus(200);
        $response->assertJson(['data' => $validate]);
    }

    /**
     * Test to assert if an existing patient
     * can be deleted.
     *
     * @test
     * @return void
     */
    public function canDropAPatient()
    {
        $patient = factory('App\Patient')->create();

        $response = $this->delete("/api/v1/patients/$patient->id");

        $response->assertStatus(204);
        $this->assertSoftDeleted('patients', ["id" => $patient->id]);
    }

    /**
     * Test for return all patients
     * exsisting in database.
     *
     * @test
     * @return void
     */
    public function canReturnAllPatients()
    {
        $patients = factory('App\Patient', 5)->create();

        $response = $this->get("/api/v1/patients");

        $response->assertJsonStructure([
            "data" => [
                ["id", "name", "sex", "age", "email"],
            ], "links", "meta",
        ]);

        $response->assertStatus(200);
    }
}
