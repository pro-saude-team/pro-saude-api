<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class DoctorTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
        $this->withoutExceptionHandling();
    }
    /**
     * Test for creating a doctor
     *
     * @test
     * @return void
     */
    public function canCreateADoctor()
    {
        $clinic = factory('App\Clinic')->create();
        $doctor = factory('App\Doctor')->make([
            'clinic_id' => 1
        ]);

        $response = $this->post('/api/v1/doctors', $doctor->toArray());

        $data = $doctor->toArray();
        unset($data["clinic_id"]);
        $data["clinic"] = $response->decodeResponseJson()["clinic"];

        $response->assertStatus(201);
        $response->assertJson($data);
        $this->assertDatabaseHas('doctors', $doctor->toArray());
    }

    /**
     * Test to return a specific doctor that has
     * been updated.
     *
     * @test
     * @return void
     */
    public function canUpdateADoctor()
    {
        $doctor = factory('App\Doctor')->create();


        $doctor->name = $doctor->name . "_updated";
        $doctor->email = $doctor->email . "_updated";

        $validate = $doctor->toArray();

        $clinic = $doctor->clinic->toArray();
        unset($clinic['deleted_at']);
        unset($clinic['created_at']);
        unset($clinic['updated_at']);

        $validate['clinic'] = $clinic;
        unset($validate['clinic_id']);
        unset($validate['created_at']);
        unset($validate['updated_at']);

        $response = $this->put("/api/v1/doctors/$doctor->id", $doctor->toArray());

        $response->assertStatus(200);
        $response->assertJson(['data' => $validate]);
        $this->assertDatabaseHas('doctors', [
                                                'name' => $validate['name'],
                                                'email' => $validate['email'],
                                            ]);
    }

    /**
     * Test for return a specific doctor 
     * exsisting in database.
     *
     * @test
     * @return void
     */
    public function canReturnADoctor()
    {
        $doctor = factory('App\Doctor')->create();

        $clinic = $doctor->clinic->toArray();
        unset($clinic['deleted_at']);
        unset($clinic['created_at']);
        unset($clinic['updated_at']);

        $validate = $doctor->toArray();

        $validate['clinic'] = $clinic;
        unset($validate['clinic_id']);
        unset($validate['created_at']);
        unset($validate['updated_at']);

        $response = $this->get("/api/v1/doctors/$doctor->id");

        $response->assertStatus(200);
        $response->assertJson(['data' => $validate]);
    }

    /**
     * Test to assert if an existing doctor
     * can be deleted.
     *
     * @test
     * @return void
     */
    public function canDropADoctor()
    {
        $doctor = factory('App\Doctor')->create();

        $response = $this->delete("/api/v1/doctors/$doctor->id");

        $response->assertStatus(204);
        $this->assertSoftDeleted('doctors', ["id" => $doctor->id]);
    }

    /**
     * Test for return all doctors
     * exsisting in database.
     *
     * @test
     * @return void
     */
    public function canReturnAllDoctors()
    {
        $doctors = factory('App\Doctor', 5)->create();

        $response = $this->get("/api/v1/doctors");

        $response->assertJsonStructure([
            "data" => [
                ["id", "name", "crm", "email", "clinic" => [
                    "id", "name", "cnes", "cnpj", "owner",
                ]],
            ], "links", "meta",
        ]);

        $response->assertStatus(200);
    }
}
