<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ScheduleTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
        $this->withoutExceptionHandling();
    }
    /**
     * Test for creating a schedule
     *
     * @test
     * @return void
     */
    public function canCreateASchedule()
    {
        $patient = factory('App\Patient')->create();
        $doctor = factory('App\Doctor')->create();
        $schedule = factory('App\Schedule')->make([
            'doctor_id' => 1,
            'patient_id' => 1,
        ]);

        $response = $this->post('/api/v1/schedules', $schedule->toArray());

        $data = $schedule->toArray();
        unset($data["doctor_id"]);
        unset($data["patient_id"]);
        $data["doctor"] = $response->decodeResponseJson()["data"]["doctor"];
        $data["patient"] = $response->decodeResponseJson()["data"]["patient"];

        $response->assertStatus(201);
        $response->assertJson(['data' => $data]);
        $this->assertDatabaseHas('schedules', $schedule->toArray());
    }

    /**
     * Test to return a specific schedule that has
     * been updated.
     *
     * @test
     * @return void
     */
    public function canUpdateASchedule()
    {
        $schedule = factory('App\Schedule')->create();

        $schedule->status = 0;
        $validate = $schedule->toArray();

        $doctor = $schedule->doctor->toArray();
        unset($doctor['deleted_at']);
        unset($doctor['created_at']);
        unset($doctor['updated_at']);
        unset($doctor['clinic_id']);

        $doctor_clinic = $schedule->doctor->clinic->toArray();
        unset($doctor_clinic['deleted_at']);
        unset($doctor_clinic['created_at']);
        unset($doctor_clinic['updated_at']);

        $patient = $schedule->patient->toArray();
        unset($patient['deleted_at']);
        unset($patient['created_at']);
        unset($patient['updated_at']);

        $validate['doctor'] = $doctor;
        $validate['doctor']['clinic'] = $doctor_clinic;
        $validate['patient'] = $patient;

        unset($validate['doctor_id']);
        unset($validate['patient_id']);
        unset($validate['schedule_id']);
        unset($validate['created_at']);
        unset($validate['updated_at']);

        $response = $this->put("/api/v1/schedules/$schedule->id", $schedule->toArray());

        $response->assertStatus(200);
        $response->assertJson(['data' => $validate]);
        $this->assertDatabaseHas('schedules', [
                                                'status' => $validate['status'],
                                            ]);
    }

    /**
     * Test for return a specific schedule 
     * exsisting in database.
     *
     * @test
     * @return void
     */
    public function canReturnASchedule()
    {
        $schedule = factory('App\Schedule')->create();
        $validate = $schedule->toArray();

        $doctor = $schedule->doctor->toArray();
        unset($doctor['deleted_at']);
        unset($doctor['created_at']);
        unset($doctor['updated_at']);
        unset($doctor['clinic_id']);

        $doctor_clinic = $schedule->doctor->clinic->toArray();
        unset($doctor_clinic['deleted_at']);
        unset($doctor_clinic['created_at']);
        unset($doctor_clinic['updated_at']);

        $patient = $schedule->patient->toArray();
        unset($patient['deleted_at']);
        unset($patient['created_at']);
        unset($patient['updated_at']);

        $validate['doctor'] = $doctor;
        $validate['doctor']['clinic'] = $doctor_clinic;
        $validate['patient'] = $patient;

        unset($validate['doctor_id']);
        unset($validate['patient_id']);
        unset($validate['schedule_id']);
        unset($validate['created_at']);
        unset($validate['updated_at']);

        $response = $this->get("/api/v1/schedules/$schedule->id");

        $response->assertStatus(200);
        $response->assertJson(['data' => $validate]);
    }

    /**
     * Test to assert if an existing schedule
     * can be deleted.
     *
     * @test
     * @return void
     */
    public function canDropASchedule()
    {
        $schedule = factory('App\Schedule')->create();

        $response = $this->delete("/api/v1/schedules/$schedule->id");

        $response->assertStatus(204);
        $this->assertSoftDeleted('schedules', ["id" => $schedule->id]);
    }

    /**
     * Test for return all schedules
     * exsisting in database.
     *
     * @test
     * @return void
     */
    public function canReturnAllSchedules()
    {
        $schedules = factory('App\Schedule', 5)->create();

        $response = $this->get("/api/v1/schedules");

        $response->assertJsonStructure([
            "data" => [
                ["id", "date", "status",

                    "patient" => [
                        "id", "name", "sex", "age", "email"
                    ],

                    "doctor" => [
                        "id", "name", "crm", "email",
                        "clinic" => [
                            "id", "name", "cnes", "cnpj", "owner",
                        ]
                    ],
                ],
            ], "links", "meta",
        ]);

        $response->assertStatus(200);
    }
}
