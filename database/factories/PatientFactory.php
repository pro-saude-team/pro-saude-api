<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Patient;
use App\Clinic;
use Faker\Generator as Faker;

$factory->define(Patient::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'age' => $faker->numberBetween(0, 150),
        'sex' => $faker->numberBetween(0, 1),
        'email' => $faker->email, 
    ];
});
