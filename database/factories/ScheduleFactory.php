<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Schedule;
use App\Doctor;
use App\Patient;
use Faker\Generator as Faker;

$factory->define(Schedule::class, function (Faker $faker) {
    return [
        //'date' => $faker->date('d-m-Y', '+2 years'),
        'date' => '30-12-2019',
        'status' => $faker->numberBetween(0, 2),
        'doctor_id' => function () {
            return factory(Doctor::class)->create()->id;
        },
        'patient_id' => function () {
            return factory(Patient::class)->create()->id;
        }
    ];
});
