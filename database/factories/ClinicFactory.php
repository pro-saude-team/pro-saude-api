<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Clinic;
use Faker\Factory as Factory;
use Faker\Generator as Faker;

$fakerBR = Factory::create("pt_BR");
$factory->define(Clinic::class, function (Faker $faker) use ($fakerBR) {
    return [
        'name' => $faker->company,
        'cnes' => $faker->numerify('#######'),
        'owner' => $faker->name, 
        'cnpj' => $fakerBR->cnpj(false),
    ];
});
