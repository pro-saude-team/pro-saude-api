<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Doctor;
use App\Clinic;
use Faker\Generator as Faker;

$factory->define(Doctor::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'crm' => $faker->numerify('#######'),
        'email' => $faker->email, 
        'clinic_id' => function () {
            return factory(Clinic::class)->create()->id;
        }
    ];
});
